module Utils (
    match,
    getAllMatch,
    doesPathMatch
) where

-- Recursive part of lengthNoStars
lengthNoStarsRec :: String -> Int -> Int -> Int
lengthNoStarsRec str i len
    | i >= len = 0
    | (str!!i) == '*' = lengthNoStarsRec str (i + 1) len
    | otherwise = 1 + lengthNoStarsRec str (i + 1) len

-- Recursive function to add indentation level
getIndentRec :: Int -> String -> Int -> String
getIndentRec iMax str i
    | i >= iMax = str
    | otherwise = ['\t'] ++ getIndentRec iMax str (i + 1)

-- Recursive part of match
matchRec :: FilePath -> String -> Int -> Int -> Int -> Int -> Bool
matchRec path match i j lenP lenM
    | i >= lenP && j >= lenM = True
    | i >= lenP || j >= lenM = False
    | (match!!j) == '*' && (j + 1) >= lenM = True
    | (match!!j) == '*' && (matchRec path match (i) (j + 1) lenP lenM) == False = matchRec path match (i + 1) j lenP lenM
    | (match!!j) == '*' = True
    | (path!!i) == (match!!j) = matchRec path match (i + 1) (j + 1) lenP lenM
    | otherwise = False

-- Recursive part of doesPathMatch
doesPathMatchRec :: FilePath -> [String] -> Int -> Int -> Bool
doesPathMatchRec path tabMatch i len
    | i >= len = False
    | match path (tabMatch!!i) == True = True
    | otherwise = doesPathMatchRec path tabMatch (i + 1) len

-- Recursive part of getAllMatch
getAllMatchRec :: [FilePath] -> [String] -> [FilePath] -> Int -> Int -> [FilePath]
getAllMatchRec tabPath tabMatch ret i len
    | i >= len = ret
    | (doesPathMatch (tabPath!!i) tabMatch) == False = getAllMatchRec tabPath tabMatch ret (i + 1) len
    | otherwise = getAllMatchRec tabPath tabMatch (ret ++ [tabPath!!i]) (i + 1) len

-- this functions returns the length of the string without counting the
-- stars inside
lengthNoStars :: String -> Int
lengthNoStars str = lengthNoStarsRec str 0 (length str)

-- this function checks if two strings matches
-- the second argument can have stars to replace every character (even void)
match :: FilePath -> String -> Bool
match path match
    | (length match) == 0 && (length path) > 0 = False
    | (lengthNoStars match) == 0 = True
    | otherwise = matchRec path match 0 0 (length path) (length match)

-- check if the given path matches with an array of file types
-- the array given can be or extensions or patterns
doesPathMatch :: FilePath -> [String] -> Bool
doesPathMatch path tabMatch = doesPathMatchRec path tabMatch 0 (length tabMatch)

-- returns an array containing all files that have match with the array of pattern given
getAllMatch :: [FilePath] -> [String] -> [FilePath]
getAllMatch tabPath tabMatch = getAllMatchRec tabPath tabMatch [] 0 (length tabPath)
