module Args (
    treatArgs
) where

import DirGest

treatArgs :: [String] -> IO ()
treatArgs tab
    | (length tab) == 0 = putStrLn "No args"
    | otherwise = putStrLn "Lots of args"
