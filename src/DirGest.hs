module DirGest (
    scanDir
) where

import System.Directory
import Control.Monad (filterM)
import System.FilePath
import Utils

fileCheck :: FilePath -> IO ()
fileCheck path
    | (doesPathMatch path ["*.o", "*~", "*.gcno", "*.gcda", "*.gch", "*.tmp", "vgcore.*", "*.gcov", "*.swp"]) == False = return ()
    | otherwise = putStrLn (path ++ " matches and would be deleted")

fileRec :: [FilePath] -> Int -> Int -> IO ()
fileRec files i len
    | i >= len = return ()
    | otherwise = do
        fileCheck (files!!i)
        fileRec files (i + 1) len

dirRec :: [FilePath] -> Int -> Int -> IO ()
dirRec dir i len
    | i >= len = return ()
    | otherwise = do
        valid <- pathIsSymbolicLink (dir!!i)
        scanDir (dir!!i) valid
        dirRec dir (i + 1) len

fileAndDir :: [FilePath] -> [FilePath] -> IO ()
fileAndDir files dir = do
    fileRec files 0 (length files)
    dirRec dir 0 (length dir)

getAllDirs :: FilePath -> [FilePath] -> IO [FilePath]
getAllDirs path allFiles = filterM (doesDirectoryExist . (path </>)) allFiles

getAllFiles :: FilePath -> [FilePath] -> IO [FilePath]
getAllFiles path allFiles = filterM (doesFileExist . (path </>)) allFiles

scanDir :: String -> Bool -> IO ()
scanDir path valid
    | valid == False = do
        --putStrLn ("switching to " ++ realPath)
        setCurrentDirectory path
        allFiles <- listDirectory "."
        dir <- getAllDirs "." allFiles
        files <- getAllFiles "." allFiles
        fileAndDir files dir
        setCurrentDirectory ".."
        --putStrLn ("moving out of " ++ realPath)
    | otherwise = return ()
